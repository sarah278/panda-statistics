#!/bin/bash

[ -f ./panda/db/panda.sqlite3 ] && mv ./panda/db/panda.sqlite3 ./panda/db/panda.sqlite3.bak
sqlite3 ./panda/db/panda.sqlite3 < ./panda/db/schema.sql
[ -f ./panda/db/panda.sqlite3 ] && echo "Done."
