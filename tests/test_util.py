"""Tests for the util.py module."""


import datetime
import pytest
from telegram import User
from panda import util


DATE = datetime.datetime.now()


def test_get_sender_first_name(update):
    """Test if get_sender returns the first_name."""
    sender = util.get_sender(update)
    assert sender.first_name == "Christian"


def test_get_sender_username(update):
    """Test if get_sender returns the first_name."""
    sender = util.get_sender(update)
    assert sender.username == "cri5h"


def test_get_sender_without_username(update):
    """Test if get_sender returns default username if the user
    doesn't have an username."""
    update._effective_user = User(id=1, first_name="cri5h", is_bot=False)
    sender = util.get_sender(update)
    assert sender.username == "(no username)"


def test_get_sender_throw_exception():
    """Test if get_sender throws an exception."""
    with pytest.raises(AttributeError):
        sender = util.get_sender(None)  # pylint: disable=W0612
