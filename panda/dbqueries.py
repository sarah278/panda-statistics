"""All database inquiries."""


from collections import namedtuple
from sqlite3 import Error as DB_Error
from telegram.utils.helpers import escape_markdown
from panda.dbhelper import DBHelper


def db_add_message(group_id, user_id_hash, msg_type, msg_length, timestamp):
    """Add a new entry to the Message table."""
    try:
        DBHelper().sql_add_message(
            group_id, user_id_hash, msg_type, msg_length, timestamp
        )
    except (DB_Error, ValueError) as db_error:
        raise ValueError(db_error)


def db_add_group(group_id, group_name=None):
    """Add a group to the table Telegram_Group."""
    try:
        DBHelper().sql_add_group(group_id, group_name)
    except DB_Error as db_error:
        raise ValueError(db_error)


def db_add_user(user_id_hash, user_name):
    """Add a Telegram user to the database."""
    try:
        DBHelper().sql_add_user(user_id_hash, user_name)
    except DB_Error as db_error:
        raise ValueError(db_error)


def db_get_username(user_id_hash):
    """Fetch the current Telegram User username."""
    try:
        username = DBHelper().sql_get_username(user_id_hash)
    except DB_Error as db_error:
        raise ValueError(db_error)

    return username[0]


def db_update_username(user_id_hash, username):
    """Update the Telegram User first name in db."""
    try:
        old_user_name = db_get_username(user_id_hash)
        if username != old_user_name:
            DBHelper().sql_update_username(user_id_hash, username)
    except DB_Error as db_error:
        raise ValueError(db_error)

    print("Old_user_name:", old_user_name)
    return old_user_name


def db_get_user_messages(user_id_hash, group_id=None):
    """Fetch the number of user messages from one group or if
    the group_id is omitted from all groups in which the user is.

    Args:
        user_id_hash (str)    : Hash of the Telegram User ID
        group_id (int or None): Telegram Group ID or None

    Returns:
        First element of the tuple from the database query (int).

    """
    try:
        if group_id:
            total_msg = DBHelper().sql_get_user_messages_from_group(
                user_id_hash, group_id
            )
        else:
            total_msg = DBHelper().sql_get_all_user_messages(user_id_hash)
    except DB_Error as db_error:
        raise DB_Error(db_error)

    return total_msg[0]


def db_query_dispatcher(func, group_id=None, timespan=0):
    """Distribute similar database queries.

    Args:
        func (function): function to call
        group_id (int): Telegram group ID or None for all groups
        timespan (int): The timespan (value 0 - 3)

    Returns:
        Depends on the function that is called. See caller function.

    """
    if timespan not in range(0, 4):
        raise ValueError(f"{timespan}: Valid numbers for timespan are 0 - 3.")

    sql_timespan = {
        0: " AND date(timestamp)>=date('now', 'start of month')",
        1: " AND date(timestamp)>=date('now','-30 day')",
        2: " AND date(timestamp)=date('now')",
        3: "",  # all
    }

    try:
        if group_id:
            result = func(group_id, sql_timespan[timespan])
        else:
            result = func(sql_timespan[timespan])
    except DB_Error as db_error:
        raise DB_Error(db_error)

    return result


def db_get_all_messages(group_id=None, timespan=0):
    """Fetch the number of all messages from one group or from
    all groups if group_id is omitted.

    Args:
        group_id (int or None): Telegram Group ID or None
        timespan (int):

    Returns:
        First element of the tuple from the database query.

    """
    if group_id:
        func = DBHelper().sql_get_all_messages_from_group
    else:
        func = DBHelper().sql_get_all_messages

    total_msg = db_query_dispatcher(func, group_id=group_id, timespan=timespan)

    return total_msg[0]


def db_get_message_types(group_id=None, timespan=0):
    """Query the different message types either from one group
    or from all messages if the group_id is omitted.

    Args:
        group_id (int or None): Telegram Group ID or None
        timestamp (int):

    Returns:
        Total messages (int)
        Number of messages (int) with message types (str) (list of tuples)

    """
    if group_id:
        func = DBHelper().sql_get_message_types_from_group
    else:
        func = DBHelper().sql_get_all_message_types

    msg_types = db_query_dispatcher(func, group_id, timespan)

    #
    # Check for division by zero!!!
    #
    total_msg = db_get_all_messages(group_id, timespan)

    return total_msg, msg_types


def db_get_top_posters(group_id=None, timespan=0):
    """Get the top posters in a group or from all groups if group_id
    is omitted.

    Args:
        group_id (int or None): Telegram Group ID or None
        timespan (int):

    Returns:
        Number of messages (int) with usernames (str) (list of tuples)

    """
    if group_id:
        func = DBHelper().sql_get_top_posters_from_group
    else:
        func = DBHelper().sql_get_top_posters_overall

    top_posters = db_query_dispatcher(func, group_id, timespan)

    return top_posters


def db_get_number_of_different_groups(network_id=0):
    """Get the number of different groups.

    Args:
        network_id (int): ID of a group composite, 0 for all groups

    Returns:
        Total of groups (int)

    """
    try:
        sum_groups = DBHelper().sql_get_number_of_different_groups(network_id)
    except DB_Error as db_error:
        raise DB_Error(db_error)

    return sum_groups[0]


def db_get_multiple_groups_info(network_id=0):
    """Get group IDs and names from table Message.

    Get either all group IDs and names or the IDs and names
    of groups within a group composite from table Message.

    Args:
        network_id (int): ID of a group composite, 0 for all groups

    Returns:
        List of Tuple with group IDs and names

    """
    try:
        groups = DBHelper().sql_get_multiple_groups_info(network_id)
    except DB_Error as db_error:
        raise DB_Error(db_error)

    return groups


def db_get_group_info_by(group_id):
    """Get all infos about a stored group.

    Args:
        group_id (int): Telegram group ID

    Returns:
        Named Tuple, with markdown escaped group name (str) and
        group ID (int)

    """
    try:
        group = DBHelper().sql_get_group_info_by(group_id)
    except DB_Error as db_error:
        raise DB_Error(db_error)

    GroupInfo = namedtuple("GroupInfo", ["id", "name", "network_id"])

    group_info = GroupInfo(
        id=group[1], name=escape_markdown(group[2]), network_id=group[3]
    )

    return group_info
