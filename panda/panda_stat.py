#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=W0613


"""
    pandastat_bot version 0.0.1

"""

import sys

# import pprint
import i18n

from telegram import ParseMode, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (
    Updater,
    Filters,
    CommandHandler,
    MessageHandler,
    CallbackQueryHandler,
)
from telegram.utils.helpers import escape_markdown, effective_message_type
from dbqueries import (
    db_get_all_messages,
    db_get_user_messages,
    db_add_message,
    db_get_message_types,
    db_get_top_posters,
    db_get_multiple_groups_info,
    db_get_group_info_by,
    db_update_username,
)
from panda.util import (
    init_logging,
    get_sender,
    add_user,
    hash_uid,
    init_groups,
    restricted,
    group_chat_only,
    selected_groups_only,
    selected_messages_only,
)
from panda.config import (
    TELEGRAM_BOT_TOKEN,
    BOT_VERSION,
    BOT_LANGUAGE,
    PUB_IP,
    CERT,
    PRIV_KEY,
)


# Debug Mode default Off
debug = False  # pylint: disable=C0103

# Init logging
LOGGER = init_logging()


@selected_groups_only
@selected_messages_only
def process_message(update, context):
    """Process every new update."""
    # pp = pprint.PrettyPrinter(indent=4)
    # pp.pprint(update.to_dict())

    group_id = update.effective_chat.id
    user_id = update.effective_user.id
    sender = get_sender(update)
    msg_type = effective_message_type(update)
    text = update.effective_message.text

    if text:
        msg_length = len(text.split())
    else:
        msg_length = 0

    timestamp = update.effective_message.date

    if debug:
        debug_msg = (
            "DEBUG: ON\n\n"
            f"Group ID: {group_id}\n"
            f"Type:{msg_type}\n"
            f"Name:@{sender.username}\n"
            f"ID:{user_id}\n"
            f"Length:{msg_length}\n"
            f"Date:{timestamp}\n"
        )
        context.bot.send_message(chat_id=update.effective_chat.id, text=debug_msg)

    try:
        db_add_message(group_id, hash_uid(user_id), msg_type, msg_length, timestamp)
    except ValueError as err:
        print(f"An error has occurred: {err}\nTrying to add new user.")
        add_user(hash_uid(user_id), sender.first_name)


def update_username(user_id_hash, sender):
    """Update the Telegram User first name in the database.

    Note: Since the usernames change from time to time, the /me command
    can be used to show the users statistics and to update the username
    in the database at the same time.

    """
    print("Sender.first_name: ", sender.first_name)
    return sender.first_name != db_update_username(user_id_hash, sender.first_name)


def user_statistic(update, context):
    """Outputs the statistics of the user either from the current group,
    or from all groups if the bot command is send in a private chat. """
    timespan = 3  # get _all_ messages

    if update.effective_chat.type == "private":
        group_id = None
    else:
        group_id = update.effective_chat.id

    user_id_hash = hash_uid(update.effective_user.id)
    sender = get_sender(update)
    username = escape_markdown(sender.username)

    if update_username(user_id_hash, sender):
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"I've changed your name to {escape_markdown(sender.first_name)}"
        )

    total_msg = db_get_all_messages(group_id, timespan=timespan)
    user_msg = db_get_user_messages(user_id_hash, group_id)

    if group_id:
        if total_msg > 0:
            text = (
                f"@{username}:\n{user_msg}/{total_msg} {i18n.t('msg', count = 2)} "
                f"({user_msg/total_msg*100:.1f}%)"
            )
        else:
            text = f"@{username}:\n{i18n.t('no_msg_grp_in_db')}"
    else:
        text = f"{user_msg} {i18n.t('msg_in_all_groups', count = user_msg)}"

    context.bot.send_message(
        chat_id=update.effective_chat.id, text=text, parse_mode=ParseMode.MARKDOWN
    )


def set_message_header(group_id, timespan, total_msg):
    """Build the header for the statistics message.

    Args:
        group_id (int|None): Telegram group ID or None for all groups
        timespan (int):

    Return:
        A formatted header (str)

    """
    time_spec = {
        0: i18n.t("this_month"),
        1: i18n.t("last_30_days"),
        2: i18n.t("today"),
        3: i18n.t("total"),
    }

    if group_id is not None:
        text = f"*{total_msg} {i18n.t('total_msg', count = total_msg)}* "
    else:
        text = f"*{total_msg} {i18n.t('msg_in_all_groups', count = total_msg)}* "

    return text + f"_({time_spec[timespan]})_\n\n"


def get_timed_messages_by(group_id, timespan):
    """Build the total statistics message.

    Args:
        group_id (int|None): The Telegram group ID or None for all groups
        timespan (int):

    Returns:
        text (str): The complete message with the statistics

    """
    total_msg = db_get_all_messages(group_id, timespan)
    text = set_message_header(group_id, timespan, total_msg)

    total_msg, msg_types = db_get_message_types(group_id, timespan)
    for posts, msg_type in msg_types:
        text += (
            f"`{posts:<4} - {i18n.t(msg_type).title():>10} "
            f"({posts/total_msg*100:>4.1f}%)`\n"
        )

    text += "\n\n*Highscore*:\n\n"

    for posts, user in db_get_top_posters(group_id, timespan):
        # user = escape_markdown(user)
        text += f"`{user}` {posts} {i18n.t('msg', count = posts)}\n"

    return text


def init_btn_state(context, msg_id, **kwargs):
    """Initialize btn_state with default or with passed values
    if the dict does not yet exist.

    Args:
        msg_id (int): Telegram message ID
        kwargs (dict): keys, values for the btn_state

    Returns:
        Either btn_state or chat_data[msg_id] if it exist (dict)

    """
    btn_state = {
        "time": 0,
        "group_no": 0,
        "groups": [],
        "extended": False,
        "show_group": False,
    }

    btn_state.update(kwargs)

    return context.chat_data.get(msg_id, btn_state)


def fetch_group_id(update, context, msg_id):
    """Get the Telegram group ID depending on the command that was send
    and the value stored in chat_data.

    Args:
        msg_id (int): Telegram message ID, either from Message object
                      or from the CallbackQuery object
        chat_data (dict): A dictionary to store chat related stuff

    Returns:
        Either the current Telegram group ID (int) or None

    """
    btn_state = init_btn_state(context, msg_id)

    if btn_state["extended"] or "/networkstat" in update.effective_message.text:
        group_id = None
    else:
        group_id = update.effective_chat.id

    return group_id


def build_markup(btn_state):
    """Build the reply_markup.

    Args:
        button_state (int): Number 0-3 for the time span that is displayed

    Returns:
        InlinekeyboardMarkup with the button(s)

    """
    fwd_button = InlineKeyboardButton(">", callback_data="forward")
    bck_button = InlineKeyboardButton("<", callback_data="backward")

    keyboard = [[bck_button, fwd_button]]
    # mdl_btn_position = 1

    if btn_state["show_group"]:
        mdl_btn_text = f"[{i18n.t('timespan')}]"
        if btn_state["group_no"] == (len(btn_state["groups"]) - 1):
            keyboard = [[bck_button]]
            # mdl_btn_position = 0
        if btn_state["group_no"] == 0:
            keyboard = [[fwd_button]]
    else:
        mdl_btn_text = f"[{i18n.t('group', count = 2)}]"
        if btn_state["time"] == 3:
            keyboard = [[bck_button]]
            # mdl_btn_position = 0
        if btn_state["time"] == 0:
            keyboard = [[fwd_button]]

    if btn_state["extended"]:
        mdl_button = InlineKeyboardButton(mdl_btn_text, callback_data="switch_context")
        # keyboard[0].insert(mdl_btn_position, mdl_button)
        keyboard.append([mdl_button])

    return InlineKeyboardMarkup(keyboard)


@group_chat_only
def total_statistics(update, context):
    """Outputs the total statistics, either from the current group
    or from all groups.

    Args:
        group_id (int|None): Telegram Group ID or None if called via
                             the /networkstat command

    """
    group_id = fetch_group_id(update, context, update.message.message_id)

    btn_state = init_btn_state(
        context, update.message.message_id, extended=not group_id
    )

    reply_markup = build_markup(btn_state)

    stat_message = get_timed_messages_by(group_id, timespan=0)

    send = context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=stat_message,
        parse_mode=ParseMode.MARKDOWN,
        reply_markup=reply_markup,
    )

    # The /networkstat command calls up the statistics for all groups.
    # The message ID is then saved in chat_data to be able to
    # identify the message again.
    if not group_id:
        btn_state = init_btn_state(context, send.message_id)
        btn_state["extended"] = True
        context.chat_data[send.message_id] = btn_state


def update_state(context, btn_pressed, msg_id):
    """Update the state of the message.

    This includes the status of the forward/backward buttons and the
    context of the message (time span or group).

    Args:
        btn_pressed (str): Telegram CallbackQuery Data
        msg_id (int): Telegram message ID

    Returns:
        Updatet btn_state (dict)

    """
    btn_state = init_btn_state(context, msg_id)

    if btn_pressed == "switch_context":
        btn_state["show_group"] = not btn_state["show_group"]

    btn_context = "group_no" if btn_state["show_group"] else "time"

    if btn_pressed == "forward":
        btn_state[btn_context] += 1
    elif btn_pressed == "backward":
        btn_state[btn_context] -= 1

    context.chat_data[msg_id] = btn_state

    return btn_state


def button_pressed(update, context):
    """Handle the forward/backward button of the statistics message.
    Get the direction from the callback_query.data and the button state
    from the chat_data.

    Args:
        btn_pressed (str): "forward", "backward", "group"
        btn_state (int): A number from 0 to 3 ...

    """
    query = update.callback_query
    msg_id = query.message.message_id

    group_id = fetch_group_id(update, context, msg_id)
    btn_state = update_state(context, query.data, msg_id)

    # CallbackQueries need to be answered, even if no notification to the user
    # is needed Some clients may have trouble otherwise. See
    # https://core.telegram.org/bots/api#callbackquery
    query.answer()

    reply_markup = build_markup(btn_state)

    if btn_state["show_group"]:
        if not btn_state["groups"]:
            btn_state["groups"] = db_get_multiple_groups_info()

        group_id = btn_state["groups"][btn_state["group_no"]][0]
        group_info = db_get_group_info_by(group_id)
        message_title = f"{group_info.name}\n"
    else:
        message_title = ""

    stat_message = message_title + get_timed_messages_by(group_id, btn_state["time"])

    context.bot.editMessageText(
        chat_id=update.effective_chat.id,
        message_id=msg_id,
        text=stat_message,
        parse_mode=ParseMode.MARKDOWN,
        reply_markup=reply_markup,
    )


@restricted
def clear_statistic():  # update, context):
    """Deletes the entire statistic of the current group from the
    database!

    """
    print("This deletes the entire statistic!")


@group_chat_only
@restricted
def output_group_id(update, context):
    """Output the current group ID. Only for admins."""
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=update.message.chat_id
    )


@restricted
def toggle_debug_mode(update, context):
    """Toggle the debug mode on/off."""
    global debug  # pylint: disable=C0103,W0603
    debug = not debug
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=f"Debug Mode: {'On' if debug else 'Off'}"
    )


def print_help(update, context):
    """Outputs a brief help message."""
    help_msg = i18n.t("help_msg")
    context.bot.send_message(
        chat_id=update.effective_chat.id, text=help_msg, parse_mode=ParseMode.MARKDOWN
    )


def error(update, context):
    """Log Errors caused by Updates."""
    LOGGER.warning('Update "%s" caused error "%s"', update, context.error)


def start_local(updater):
    """Start the bot on local machine."""
    updater.start_polling()  # (timeout=30)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


def start_webhook(updater):
    """Start the bot on server."""
    updater.start_webhook(
        listen=PUB_IP,
        port=8443,
        url_path=TELEGRAM_BOT_TOKEN,
        key=PRIV_KEY,
        cert=CERT,
        webhook_url=f"https://{PUB_IP}:8443/{TELEGRAM_BOT_TOKEN}",
    )
    updater.idle()


def setup_translation(locale):
    """Loads translation files from ./lang directory into memory;
    if locale is not available, it falls back to "en"

    Args:
        locale(str): two character locale identifier
                     (e.g. "en", "it", "de", ...)
    """
    i18n.set("enable_memoization", True)
    i18n.set("filename_format", "{locale}.{format}")
    i18n.set("locale", locale)
    i18n.set("fallback", "en")
    i18n.load_path.append("./lang")


def main():
    """Start the bot."""
    # Setup translation and transfer groups from config to db
    print("Setup translation...", end="")
    setup_translation(BOT_LANGUAGE)
    print("Done.")
    init_groups()
    print(f"{BOT_VERSION[0], BOT_VERSION[1]} starting...")

    # Create EventHandler and pass it your bot's token.
    updater = Updater(token=TELEGRAM_BOT_TOKEN, use_context=True)
    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CallbackQueryHandler(button_pressed))
    dispatcher.add_handler(CommandHandler("me", user_statistic))
    dispatcher.add_handler(CommandHandler("stat", total_statistics))
    dispatcher.add_handler(CommandHandler("networkstat", total_statistics))
    dispatcher.add_handler(CommandHandler("clear", clear_statistic))
    dispatcher.add_handler(CommandHandler("gid", output_group_id))
    dispatcher.add_handler(CommandHandler("debug", toggle_debug_mode))
    dispatcher.add_handler(CommandHandler("help", print_help))
    dispatcher.add_handler(
        MessageHandler(Filters.all & ~Filters.command, process_message)
    )

    # log all errors
    dispatcher.add_error_handler(error)

    # Start polling or webhook
    if sys.argv[-1] == "webhook":
        start_webhook(updater)
    else:
        start_local(updater)


if __name__ == "__main__":
    main()
